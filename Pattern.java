import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class Pattern {
    int[][] pattern;
    int[][][] matrix;

    private int[][] create2DIntMatrixFromFile(Path path) throws IOException {
        return Files.lines(path)
                .map((l) -> l.trim().split("\\s+"))
                .map((sa)-> Stream.of(sa).mapToInt(Integer::parseInt).toArray())
                .toArray(int[][]::new);
    }

    public void printPattern(int[][] pattern)
    {
        for (int i = 0; i < pattern.length; i++)
        {
            for (int j = 0; j < pattern[i].length; j++) {
                System.out.print(pattern[i][j]);
                if ((j + 1) < pattern[i].length)
                    System.out.print(" ");
            }
            if ((i + 1) < pattern.length)
                System.out.print("\n");
        }
    }

    private int[][] create2DMatrix(int[] line)
    {
        int[][] tmpMatrix = new int[line.length][line.length];

        for (int i = 0; i < line.length; i++)
            for (int j = 0; j < line.length; j++)
                tmpMatrix[i][j] = line[i] * line[j];

        int j = 0;
        for (int i = 0; i < line.length; i++)
        {
            tmpMatrix[i][j] = 0;
            j += 1;
        }

        return (tmpMatrix);
    }

    private void create3DMatrix()
    {
        int[][][] tmp3DMatrix = new int[pattern.length][][];

        for (int i = 0; i < pattern.length; i++)
            tmp3DMatrix[i] = create2DMatrix(pattern[i]);

        matrix = tmp3DMatrix;
    }

    public  Pattern(String file) {
        try {
            pattern = create2DIntMatrixFromFile(FileSystems.getDefault().getPath(file));
            create3DMatrix();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
