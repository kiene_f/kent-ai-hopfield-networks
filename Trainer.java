public class Trainer {
    Pattern storedPattern = null;
    Pattern corruptedPattern = null;
    int[][][][] trainingMatrix;

    public void findBestPattern()
    {
        int[][][] sum = new int[trainingMatrix.length][trainingMatrix[0].length][trainingMatrix[0][0].length];
        int[] minDiffList = new int[trainingMatrix.length];

        for (int i = 0; i < trainingMatrix.length; i++)
        {
            for (int j = 0; j < trainingMatrix[i].length; j++)
                for (int k = 0; k < trainingMatrix[i][j].length; k++)
                {
                    sum[i][j][k] = 0;
                    for (int l = 0; l < trainingMatrix[i][j][k].length; l++)
                    {
                        sum[i][j][k] += trainingMatrix[i][j][k][l];
                        if (sum[i][j][k] < 0)
                            minDiffList[i] += 1;
                    }
                }
        }

        int minDiffIndex = 0;
        int minDiff = minDiffList[0];
        for (int i = 0; i < minDiffList.length; i++) {
            if (minDiffList[i] < minDiff) {
                minDiff = minDiffList[i];
                minDiffIndex = i;
            }
        }

        for (int i = minDiffIndex; i < storedPattern.pattern.length + minDiffIndex; i++)
            for (int j = 0; j < corruptedPattern.pattern[i].length; j++)
            {
                if (sum[minDiffIndex][i - minDiffIndex][j] < 0)
                    corruptedPattern.pattern[i][j] = ~(corruptedPattern.pattern[i][j] - 1);
                if (corruptedPattern.pattern[i][j] != storedPattern.pattern[i - minDiffIndex][j])
                    return;
            }

        corruptedPattern.printPattern(corruptedPattern.pattern);
    }

    public void training()
    {
        int nb = corruptedPattern.pattern.length - storedPattern.pattern.length + 1;
        int[][][][] tmpTrainingMatrix = new int[nb][storedPattern.matrix.length][storedPattern.matrix[0].length][storedPattern.matrix[0][0].length];


        for (int i = 0; i < nb; i++)
        {
            int k = 0;
            for (int j = i; j < i + storedPattern.matrix.length; j++)
            {
                for (int l = 0; l < storedPattern.matrix[k].length; l++)
                    for (int m = 0; m < storedPattern.matrix[k][l].length; m++)
                    {
                        tmpTrainingMatrix[i][k][l][m] = storedPattern.matrix[k][l][m] * corruptedPattern.matrix[j][l][m];
                    }
                k++;
            }
        }

        trainingMatrix = tmpTrainingMatrix;
    }

    public Trainer(Pattern storedPattern, Pattern corruptedPattern)
    {
        this.storedPattern = storedPattern;
        this.corruptedPattern = corruptedPattern;
        training();
        findBestPattern();
    }
}
