public class hopf {
    public static void main(String[] args) {
        Pattern storedPattern;
        Pattern corruptedPattern;

        if (args.length != 2)
            return;

        storedPattern = new Pattern(args[0]);
        corruptedPattern = new Pattern(args[1]);

        if (storedPattern.pattern[0].length != corruptedPattern.pattern[0].length)
            return;

        new Trainer(storedPattern, corruptedPattern);
    }
}
